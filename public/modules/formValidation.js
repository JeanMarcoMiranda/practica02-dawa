const emailValidation = require('./auxModules/formValidation/isEmail')
const messageValidation = require('./auxModules/formValidation/validMessage')
const phoneValidation = require('./auxModules/formValidation/validPhone')
const textValidation = require('./auxModules/formValidation/isEmpty')
const bDateValidation = require('./auxModules/formValidation/validDate')

exports.validateFields = (body) => {
  const {name, lastName, email, message, phone, bDate} = body
  const validName = textValidation.textIsValid(name.trim())
  const validLastName = textValidation.textIsValid(lastName.trim())
  const validPass = emailValidation.isEmail(email)
  const validPhone = phoneValidation.validPhone(phone.trim())
  const validBDate = bDateValidation.calcularEdad(new Date(bDate))
  const validMsg = messageValidation.validMessage(message.trim())

  let allDataIsValid, data

  if(validPass && validMsg && validPhone && validName && validLastName && validBDate){
    allDataIsValid = true
    data = {
      name: name,
      lastName: lastName,
      email: email,
      message: message,
      phone: phone,
      bDate: bDate
    }
  }else{
    allDataIsValid = false
    data = {
      errors: {
        name: validName,
        lastName: validLastName,
        email : validPass,
        message: validMsg,
        phone: validPhone,
        bDate: validBDate
      },
      bodyData: {
        name: name,
        lastName: lastName,
        email: email,
        message: message,
        phone: phone,
        bDate: bDate
      }
    }
  }

  const validationData = {validationIsSuccess : allDataIsValid, dataForm : data}

  return validationData
}