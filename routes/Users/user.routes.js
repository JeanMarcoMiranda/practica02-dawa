//Packages import
const routes = require('express').Router()

//Route modules
const {
  newUser,
  getUsers,
  deleteUser,
  updateUser
} = require('./user')

routes.route('/')
  .get(getUsers)
  .post(newUser)

routes.route('/edit/:userId')
  .put(updateUser)
  .delete(deleteUser)

module.exports = routes