const bcrypt = require('bcrypt')
const User = require('../../models/user')

module.exports = {
  
  newUser : async (req, res) => {
    try{
      const newUser = new User(req.body)
      newUser.password = bcrypt.hashSync(req.body.password, 10)

      const userSaved = await newUser.save()

      userSaved.password = null;

      res.json({
        ok: true,
        user: userSaved
      })

    }catch(error){
      res.status(400).json({
        ok: false,
        error,
      })
    }
    
  },

  getUsers : async (req, res) => {
    try{
      const users = await User.find({}).populate('posts')
      res.json({
        ok: true,
        users
      })
    }catch(error){
      res.status(400).json({
        ok: false,
        error,
      })
    }
  },

  deleteUser : async (req, res) => {
    try{
      const {userId} = req.params
      const userDeleted = await Post.findByIdAndDelete(userId)
      res.json({
        ok: true,
        userDeleted
      })
    }catch(error){
      res.status(400).json({
        ok: false,
        error
      })
    }
  },

  updateUser : async (req, res) => {
    try{
      const {userId} = req.params
      const body = req.body
      const userUpdated = await Post.findByIdAndUpdate(userId, body, {new: true})
      res.status(200).json({
        ok: true,
        message: 'User updated.',
        userUpdated
      })
    }catch(error){
      res.status(400).json({
        ok: false,
        error
      })
    }
  }
  
}