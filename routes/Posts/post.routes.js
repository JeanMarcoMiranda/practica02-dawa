const router = require('express').Router()
const {
  getPosts, 
  newPost, 
  deletePost, 
  updatePost,
  newUserPost
} = require('./post')

const {newPostComment} = require('../Comments/comment')

router.route('/')
  .get(getPosts)
  .post(newPost)


/* router.route('/:postId')
  .get(getOnePost) */


/* router.route('/:postId/comments')
  .get(getPostComents) */


router.route('/:postId/comments/:userId')
  .post(newPostComment)

/* router.route('/:postId/comments/:commentId')
  .delete(deleteCommentFromPost)
  .put(updateCommentFromPost) */

router.route('/:userId')
  .post(newUserPost)


router.route('/edit/:postId')
  .put(updatePost)
  .delete(deletePost)
module.exports = router