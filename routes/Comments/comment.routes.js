//Packages import
const routes = require('express').Router()

//Route modules
const {
  getComments,
  deleteComment,
  updateComment
} = require('./comment')

routes.route('/')
  .get(getComments)


routes.route('/edit/:userId')
  .put(updateComment)
  .delete(deleteComment)

module.exports = routes