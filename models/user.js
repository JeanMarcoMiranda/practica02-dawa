const mongoose = require('mongoose')
const schema = mongoose.Schema


const userSchema = new schema({
  name: {
    type: String,
    requided: [true, "El nombre es necesario"]
  },
  email: {
    type: String,
    unique: true,
    required: [true, "El correo es necesario"]
  },
  password: {
    type: String,
    required: [true, "El password es necesario"]
  },
  status: {
    type: Boolean,
    default: true
  },
  posts:[{
    type: schema.Types.ObjectId,
    ref: 'Post'
  }]
})


module.exports = mongoose.model('User', userSchema)